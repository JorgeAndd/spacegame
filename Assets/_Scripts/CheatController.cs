﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CheatController : MonoBehaviour {
	public PlayerController player;
	public GameController game;
	public List<GameObject> weapons = new List<GameObject>();
	private int curWeapon = 0;

	// Update is called once per frame
	void Update () {
		if(Input.GetKeyDown(KeyCode.Comma))
		{
			curWeapon--;
			if(curWeapon == -1)
				curWeapon = weapons.Count - 1;

			GameObject newWeapon = (GameObject)Instantiate(weapons[curWeapon]);
			player.changeWeapon(newWeapon);
		}

		if(Input.GetKeyDown(KeyCode.Period))
		{
			curWeapon++;
			if(curWeapon == weapons.Count)
				curWeapon = 0;
			
			GameObject newWeapon = (GameObject)Instantiate(weapons[curWeapon]);
			player.changeWeapon(newWeapon);
		}

		if(Input.GetKeyDown(KeyCode.B))
			game.enemyCounter = game.bossThreshold;
	}
}
