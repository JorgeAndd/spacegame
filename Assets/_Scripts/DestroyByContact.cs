﻿/**
 * Created by: Jorge Luiz Andrade
 * Last Update: March 01 2015
 * Purpose: Controls object destruction when hit by another
 * */

using UnityEngine;
using System.Collections;

public class DestroyByContact : MonoBehaviour 
{
	public GameObject explosion;
	public GameObject playerExplosion;
	public int scoreValue;

	private GameController gameController;

	void Start()
	{
		GameObject gameControllerObject = GameObject.FindWithTag("GameController");

		if(gameControllerObject)
		{
			gameController = gameControllerObject.GetComponent<GameController>();
		}

		if(gameController == null)
		{
			Debug.Log ("Cannot find GameController script");
		}
	}

	void OnTriggerEnter(Collider other) 
	{
		if(other.tag == "Shoot")
		{
			Instantiate(explosion, transform.position, transform.rotation);
			Destroy(other.gameObject);
			Destroy(gameObject);
			gameController.AddScore(scoreValue);
		}
	}
}
