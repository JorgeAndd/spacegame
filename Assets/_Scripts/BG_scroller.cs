﻿/**
 * Created by: Protim Roy<protim@mailuoguelph.ca>
 * Date created: 18/03/2015
 * Last updated: 30/03/2015
 * Purpose: This script is to scroll the background.
 * */

using UnityEngine;
using System.Collections;

public class BG_scroller : MonoBehaviour {

	public float scrollSpeed = 0.5F;

	void Update(){
		//Update the offset value based on time and scrollSpeed.
		float offset = Time.time * scrollSpeed/15;
		//Sent the new material
		renderer.material.SetTextureOffset("_MainTex", new Vector2(0, -offset));
	}
}
