﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class Hazard : System.Object
{
	public GameObject enemy;
	public int weight;
}

public class EnemySpawner : MonoBehaviour {
	public List<Hazard> hazards = new List<Hazard>();
	private int totalWeight;

	void Awake()
	{
		foreach (Hazard hazard in hazards)
			totalWeight += hazard.weight;
	}

	public GameObject generateEnemy()
	{
		int rand = Random.Range(0, totalWeight + 1);

		foreach (Hazard hazard in hazards)
		{
			rand -= hazard.weight;
			if(rand <= 0)
			{
				return hazard.enemy;
			}
		}

		Debug.Log ("Error");
		return null;
	}
}
