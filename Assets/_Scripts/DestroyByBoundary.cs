﻿/**
 * Created by: Jorge Luiz Andrade
 * Last Update: March 01 2015
 * Purpose: Destroy an object when it exits the game boundaries
 * */

using UnityEngine;
using System.Collections;

public class DestroyByBoundary : MonoBehaviour 
{
	void OnTriggerExit(Collider other) 
	{
		Destroy(other.gameObject);
	}
}
