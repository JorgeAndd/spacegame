﻿/**
 * Created by: Jorge Luiz Andrade
 * Last Update: March 01 2015
 * Purpose: Rotates an object in a random angle
 * */

using UnityEngine;
using System.Collections;

public class RandomRotator : MonoBehaviour 
{
	public float tumble;

	void Start()
	{
		rigidbody.angularVelocity = Random.insideUnitSphere * tumble;
	}
}
