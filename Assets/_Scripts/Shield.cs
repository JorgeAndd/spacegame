﻿/**
 * Created by: Jorge Luiz Andrade
 * Last Update: March 17 2015
 * Purpose: Basic shield script. Just to store shield value
 * */

using UnityEngine;
using System.Collections;

public class Shield : MonoBehaviour {
	public int shieldValue;	
}
