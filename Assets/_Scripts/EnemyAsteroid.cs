﻿/**
 * Created by: Jorge Luiz Andrade
 * Last Update: March 25 2015
 * Purpose: Basic asteroid
 * */

using UnityEngine;
using System.Collections;

public class EnemyAsteroid : Enemy {
	protected override void onDamage (int damage)
	{
		this.hp -= damage;
		if(hp <= 0)
			onDeath();
	}
	
	protected override void onPlayerHit()
	{
		onDeath();
		playerController.OnDamage(40);
	}
	
	protected override void onDeath()
	{
		Instantiate (enemyExplosion, transform.position, transform.rotation);
		Destroy (gameObject);
		gameController.AddScore(scoreValue);
	}
}
