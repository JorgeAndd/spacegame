﻿/**
 * Created by: Jorge Luiz Andrade and Protim Roy
 * Last Update: April 2nd 2015
 * Purpose: Main game controller
 * */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.IO;

public class GameController : MonoBehaviour 
{
	public GameObject boss;
	public Vector3 spawnValues;
	public float spawnWait;
	public float startWait;
	private EnemySpawner enemySpawner;
	
	public int enemyCounter;
	public int bossThreshold;
	private bool spawnedBoss;
	
	public Text scoreText;
	public Text highScoreText;
	public Text restartText;
	public Text gameOverText;
	public Text saveGameText;
	public Text MainMenuText;
	public Text shieldText;
	public Text weaponText;
	private int score;
	
	private bool gameOver;
	private bool mainMenu;
	
	void Start()
	{
		gameOver = false;
		
		enemySpawner = gameObject.GetComponent<EnemySpawner>();
		restartText.text = "";
		gameOverText.text = "";
		score = 0;
		UpdateScore();

		highScoreText.text = getHighScore().ToString();

		StartCoroutine("SpawnWaves");
	}
	
	void Update()
	{
		if(gameOver)
		{
			StopCoroutine("SpawnWaves");
			restartText.text = "Press 'R' for Restart";
			MainMenuText.text = "Press 'Q' for main menu";
			saveGameText.text = "Press 'W' to save your score";
			if (Input.GetKeyDown(KeyCode.R)){
				gameOver = false;
				
				Application.LoadLevel(Application.loadedLevel);
			}
			if(Input.GetKeyDown(KeyCode.Q)){
				gameOver = true;
				
				Application.LoadLevel(0);
			}
			if(Input.GetKeyDown(KeyCode.W)){
				gameOver = true;
				/*Open or create a file.*/
				string path = Application.dataPath + "/highscore.txt";
				
				if(!File.Exists(path)){
					File.Create(path);
					TextWriter tw = new StreamWriter(path);
					tw.WriteLine("Highscore: " + score);
					tw.Close();
				}
				else if(File.Exists (path)){
					TextWriter tw = new StreamWriter(path, true);
					tw.WriteLine ("Highscore: " + score);
					tw.Close ();
				}
			}
		}
		
		if(enemyCounter >= bossThreshold && !spawnedBoss)
		{
			StopCoroutine("SpawnWaves");
			spawnedBoss = true;
			Instantiate(boss, new Vector3(12, 0, 0), boss.transform.rotation);
		}
	}
	
	private IEnumerator SpawnWaves()
	{
		yield return new WaitForSeconds(startWait);
		
		while(true)
		{
			GameObject newHazard = enemySpawner.generateEnemy();
			
			Vector3 spawnPosition = new Vector3(spawnValues.x,spawnValues.y, 
			                                    Random.Range(spawnValues.z, -spawnValues.z));
			Instantiate(newHazard, spawnPosition, newHazard.transform.rotation);
			yield return new WaitForSeconds(startWait);
		}
		
	}
	
	public void AddScore(int newScoreValue)
	{
		score += newScoreValue;
		UpdateScore();
	}
	
	void UpdateScore()
	{
		scoreText.text = "Score: " + score;
	}
	
	public void UpdateShieldText(int shield)
	{
		shieldText.text = "Shield " + shield + "%";
	}
	
	public void UpdateWeapon(string weaponName)
	{
		weaponText.text = weaponName;
	}
	
	public void GameOver()
	{
		gameOverText.text = "Game Over";
		gameOver = true;
	}

	private int getHighScore()
	{
		int highScore = 0;
		string path = Application.dataPath + "/highscore.txt";

		TextReader file = new StreamReader(path);

		string line;
		while((line = file.ReadLine()) != null)
		{
			int score = int.Parse(line.Substring(10));

			if(score > highScore)
				highScore = score;
		}

		return highScore;
	}
}
