﻿/**
 * Created by: Jorge Luiz Andrade
 * Last Update: March 26 2015
 * Purpose: Rapid weapon. Fires shots more frequently than the basic weapon
 * */

using UnityEngine;
using System.Collections;

public class RapidWeapon : Weapon {
	
	void Start()
	{
		shotMover = (Mover)shot.GetComponent(typeof(Mover));
		fireRate = 0.1f;
		weaponName = "Rapid";
	}
	
	public override void Shoot()
	{
		if(Time.time > nextFire)
		{
			nextFire = Time.time + fireRate; 
			shotMover.direction = shotPosition.forward;
			Object.Instantiate(shot, shotPosition.position, Quaternion.identity);
		}
	}
}
