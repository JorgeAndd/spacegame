/**
 * Created by: Protim Roy<protim@mailuoguelph.ca>
 * Date created: 29/03/2015
 * Last updated: 31/03/2015
 * Purpose: This script is create the enemies with AI that is not that smart.
 * The enemies cannot follow the play and can only move up/down.
 * */

using UnityEngine;
using System.Collections;

public class EnemyMed: Enemy {
	public Weapon weapon;
	public float fireRate;
	public float delay;

	public override void Start()
	{
		base.Start();	
		weapon = GetComponentInChildren<Weapon>();
		this.rigidbody.velocity = new Vector3(-1,0,0.5f)*5;
		InvokeRepeating ("Shoot", delay, fireRate);
		
	}
	/*This part has been updated to include movement in the enemies*/
	void Update(){
		if (this.transform.position.z >= 9.3) {
			this.rigidbody.velocity = new Vector3(-1,0,-0.5f)*5;
		}
		else if (this.transform.position.z <=-8){
			this.rigidbody.velocity = new Vector3(-1,0,0.5f)*5;
		}
	}
	protected override void onDamage (int damage)
	{
		this.hp -= damage;
		if(hp <= 0)
			onDeath();
	}
	
	protected override void onPlayerHit()
	{
		Instantiate (enemyExplosion, transform.position, transform.rotation);
		Destroy(gameObject);
		playerController.OnDamage(30);
	}
	
	protected override void onDeath()
	{
		Instantiate (enemyExplosion, transform.position, transform.rotation);
		gameController.AddScore(scoreValue);
		Destroy(this.gameObject);
		
		GameObject drop = dropManager.generateDrop(0.5f);
		
		if(drop)
		{
			drop = (GameObject)Instantiate(drop, transform.position, transform.rotation);
			drop.rigidbody.velocity = new Vector3(-4, 0, 0);
		}

		gameController.enemyCounter++;
	}
	
	private void Shoot()
	{
		weapon.Shoot();
	}
}