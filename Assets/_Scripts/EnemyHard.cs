﻿/**
 * Created by: Protim Roy<protim@mailuoguelph.ca>
 * Date created: 29/03/2015
 * Last updated: 31/03/2015
 * Purpose: This script is create the hard enemies with AI.
 * */
using UnityEngine;
using System.Collections;

public class EnemyHard : Enemy {
	public Weapon weapon;
	public float fireRate;
	public float delay;
	public override void Start()
	{
		base.Start();	
		weapon = GetComponentInChildren<Weapon>();
		this.rigidbody.velocity = new Vector3(-1,0,0.5f)*5;
		InvokeRepeating ("Shoot", delay, fireRate);
		
	}
	/*This part has been updated. The boss now will attempt to evade the player*/
	void Update(){
		if (playerController != null) {
			if (this.transform.position.z >= playerController.transform.position.z) {
				this.rigidbody.velocity = new Vector3 (0, 0, 0.5f) * 5;
			} 
			else if (this.transform.position.z < playerController.transform.position.z) {
				this.rigidbody.velocity = new Vector3 (0, 0, 0.5f) * 5;
			}
		}
	}
	protected override void onDamage (int damage)
	{
		this.hp -= damage;
		if(hp <= 0)
			onDeath();
	}
	
	protected override void onPlayerHit()
	{
		Instantiate (enemyExplosion, transform.position, transform.rotation);
		Destroy(gameObject);
		playerController.OnDamage(30);
	}
	/*This part has been updated to destroy the items. I specifically added this to the script*/
	void OnTriggerEnter(Collider other)
	{
		if(other.tag == "Shoot")
		{
			Destroy(other.gameObject);
			int damage = other.GetComponent<Shot>().damage;
			onDamage(damage);
		}
		
		if(other.tag == "Player")
		{
			onPlayerHit();
		}
		/*Detects weapons or shields, destroy gameObject*/
		if (other.tag == "Weapon"||other.tag == "Shield") {
			Destroy(other.gameObject);
		}
	}
	protected override void onDeath()
	{
		Instantiate (enemyExplosion, transform.position, transform.rotation);
		gameController.AddScore(scoreValue);
		Destroy(this.gameObject);
		
		GameObject drop = dropManager.generateDrop(0.5f);
		
		if(drop)
		{
			drop = (GameObject)Instantiate(drop, transform.position, transform.rotation);
			drop.rigidbody.velocity = new Vector3(-4, 0, 0);
		}

		gameController.enemyCounter++;
	}
	
	private void Shoot()
	{
		weapon.Shoot();
	}
}
