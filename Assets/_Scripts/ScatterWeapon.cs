﻿/**
 * Created by: Jorge Luiz Andrade
 * Last Update: March 26 2015
 * Title: Shot.cs 
 * Purpose: Scatter weapon. Fires 3 shots
 * */

using UnityEngine;
using System.Collections;

public class ScatterWeapon : Weapon {
	
	void Start()
	{
		shotMover = (Mover)shot.GetComponent(typeof(Mover));
		fireRate = 0.5f;
		weaponName = "Scatter";
	}
	
	public override void Shoot()
	{
		if(Time.time > nextFire)
		{
			nextFire = Time.time + fireRate; 

			shotMover.direction = Quaternion.Euler(0, 15, 0) * shotPosition.forward;
			Object.Instantiate(shot, shotPosition.position, Quaternion.AngleAxis(15, Vector3.up));
			
			shotMover.direction = shotPosition.forward;
			Object.Instantiate(shot, shotPosition.position, Quaternion.AngleAxis(0, Vector3.up));
			
			shotMover.direction = Quaternion.Euler(0, -15, 0) * shotPosition.forward;
			Object.Instantiate(shot, shotPosition.position, Quaternion.AngleAxis(-15, Vector3.up));
		}
	}
}
