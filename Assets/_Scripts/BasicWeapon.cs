﻿/**
 * Created by: Jorge Luiz Andrade
 * Last Update: March 26 2015
 * Purpose: Basic game weapon
 * */

using UnityEngine;
using System.Collections;

public class BasicWeapon : Weapon {

	void Start()
	{
		shotMover = (Mover)shot.GetComponent(typeof(Mover));
		fireRate = 0.25f;
		nextFire = 0f;
		weaponName = "Basic";
	}

	public override void Shoot()
	{
		if(Time.time > nextFire)
		{
			nextFire = Time.time + fireRate; 
			shotMover.direction = shotPosition.forward;
			Object.Instantiate(shot, shotPosition.position, Quaternion.identity);
		}
	}
}
