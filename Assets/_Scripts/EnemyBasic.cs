﻿/**
 * Created by: Jorge Luiz Andrade
 * Last Update: March 26 2015
 * Purpose: Basic Enemy
 * */

using UnityEngine;
using System.Collections;

public class EnemyBasic: Enemy {

	public Weapon weapon;
	public float fireRate;
	public float delay;

	public override void Start()
	{
		base.Start();	
		weapon = GetComponentInChildren<Weapon>();
		InvokeRepeating ("Shoot", delay, fireRate);

	}

	protected override void onDamage (int damage)
	{
		this.hp -= damage;
		if(hp <= 0)
			onDeath();

		gameController.enemyCounter++;
	}
	
	protected override void onPlayerHit()
	{
		Instantiate (enemyExplosion, transform.position, transform.rotation);
		Destroy(gameObject);
		playerController.OnDamage(20);
	}
	
	protected override void onDeath()
	{
		Instantiate (enemyExplosion, transform.position, transform.rotation);
		gameController.AddScore(scoreValue);
		Destroy(this.gameObject);

		GameObject drop = dropManager.generateDrop(0.5f);

		if(drop)
		{
			drop = (GameObject)Instantiate(drop, transform.position, transform.rotation);
			drop.rigidbody.velocity = new Vector3(-4, 0, 0);
		}
	}

	private void Shoot()
	{
		weapon.Shoot();
	}




}
