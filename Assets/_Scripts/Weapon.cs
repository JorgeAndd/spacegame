﻿/**
 * Created by: Jorge Luiz Andrade
 * Last Update: March 17 2015
 * Purpose: Base abtract weapon class. Weapons classes must derived from this
 * */

using UnityEngine;
using System.Collections;

public abstract class Weapon : MonoBehaviour {
	public GameObject shot;

	public Transform shotPosition;
	public Mover shotMover;
	protected float fireRate = 0.5f;
	protected float nextFire = 0.0f;
	public string weaponName;

	//Abstract method. To be called when player fires
	public abstract void Shoot();

	public string GetName()
	{
		return weaponName;
	}
}
