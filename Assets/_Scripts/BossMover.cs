﻿/**
 * Created by: Protim Roy
 * Last Update: April 16 2015
 * Purpose: Move an object in a fixed direction and speed
 * */

using UnityEngine;
using System.Collections;

public class BossMover : MonoBehaviour 
{
	public float speed;
	public Vector3 direction;
	
	void Start()
	{
		if ((direction.x < 0)||(direction.y < 0)||(direction.z < 0)) {
			rigidbody.velocity = direction * speed;
		}
	}
}
