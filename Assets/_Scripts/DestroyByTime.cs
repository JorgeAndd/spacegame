﻿/**
 * Created by: Jorge Luiz Andrade
 * Last Update: March 01 2015
 * Purpose: Destroy an object when it's life time is over
 * */

using UnityEngine;
using System.Collections;

public class DestroyByTime : MonoBehaviour {
	public float lifetime;

	void Start () {
		Destroy(gameObject, lifetime);
	}
}
