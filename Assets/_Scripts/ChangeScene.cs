﻿/**
 * Created by: Protim Roy
 * Date Created: 22/02/2015
 * Last Update: March 01 2015
 * Title: Alt_PlayerController.cs 
 * Purpose: The purpose of this script is to change the scene. 
 * */
using UnityEngine;
using System.Collections;

public class ChangeScene : MonoBehaviour {

	// Use this for initialization
	public void ChangeToScene (int sceneToChangeTo) {
		Application.LoadLevel (sceneToChangeTo);
	}
}
