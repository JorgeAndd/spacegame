﻿/**
 * Created by: Jorge Luiz Andrade
 * Last Update: March 25 2015
 * Purpose: Generates a drop
 * */

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DropManager : MonoBehaviour{
	public List<GameObject> drops = new List<GameObject>();

	private int nDrops;

	void Start()
	{
		nDrops = drops.Count;
	}

	public GameObject generateDrop(float chance)
	{
		float rnd = Random.Range (0, 1f);

		if(rnd < chance)
			return getDrop();
		else
			return null;
	}

	private GameObject getDrop()
	{
		int rnd = Random.Range(0,nDrops);

		return drops[rnd];
	}
}
