﻿/**
 * Created by: Jorge Luiz Andrade
 * Last Update: March 26 2015
 * Purpose: Move an object in a fixed direction and speed
 * */

using UnityEngine;
using System.Collections;

public class Mover : MonoBehaviour 
{
	public float speed;
	public Vector3 direction;

	void Start()
	{
		rigidbody.velocity = direction * speed;
	}
}
