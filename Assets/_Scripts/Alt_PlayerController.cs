﻿/**
 * Created by: Protim Roy
 * Date Created: 27/02/2015
 * Last Update: March 01 2015
 * Title: Alt_PlayerController.cs 
 * Purpose: This is the script to control the player module for the alternate modes. 
 * This script changes the movement to snake like.
 * */
using UnityEngine;
using System.Collections;

[System.Serializable]
public class BoundaryAlt
{
	public float xMin, xMax, zMin, zMax;
}

public class Alt_PlayerController : MonoBehaviour 
{
	public float speed;
	public float tilt;
	public BoundaryAlt boundary;
	
	public GameObject shot;
	public Transform shotSpawn; 
	
	public float fireRate = 0.5f;
	private float nextFire = 0.0f;
	
	void Update()
	{
		if(Input.GetButton("Fire1") && Time.time > nextFire)
		{
			nextFire = Time.time + fireRate; 
			Instantiate(shot, shotSpawn.position, Quaternion.identity);
			audio.Play();
		}
	}
	
	void FixedUpdate()
	{
		float moveHorizontal = Input.GetAxis("Horizontal");
		float moveVertical = Input.GetAxis("Vertical");
		
		Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
		rigidbody.velocity = movement * speed;
		
		rigidbody.position = new Vector3
			(
				Mathf.Clamp(rigidbody.position.x, boundary.xMin, boundary.xMax),
				0.0f,
				Mathf.Clamp(rigidbody.position.z, boundary.zMax, boundary.zMin)
				);
		
		//rigidbody.rotation = Quaternion.Euler(rigidbody.velocity.z * -tilt, 90, 75);
	}
}
