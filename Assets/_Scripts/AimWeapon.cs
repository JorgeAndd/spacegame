﻿/**
 * Created by: Jorge Luiz Andrade
 * Last Update: March 26 2015
 * Purpose: Weapon that fires on mouse direction
 * */

using UnityEngine;
using System.Collections;

public class AimWeapon : Weapon {
	
	void Start()
	{
		shotMover = (Mover)shot.GetComponent(typeof(Mover));
		fireRate = 0.25f;
		weaponName = "Aim";
	}
	
	public override void Shoot()
	{
		if(Time.time > nextFire)
		{
			nextFire = Time.time + fireRate; 

			Vector2 mousePos = Input.mousePosition;
			Vector3 cursor = new Vector3(mousePos.x, mousePos.y, 0);
			cursor = Camera.main.ScreenToWorldPoint(cursor);
			cursor.y = 0;
			Vector3 direction = cursor - shotPosition.transform.position;
			direction.Normalize();
			shotMover.direction = direction;
			Object.Instantiate(shot, shotPosition.position, Quaternion.identity);
		}
	}
}
