﻿/**
 * Created by: Jorge Luiz Andrade
 * Last Update: March 26 2015
 * Purpose: Base abtract enemy class. Enemies classes must derived from this
 * */

using UnityEngine;
using System.Collections;

public abstract class Enemy : MonoBehaviour {
	public GameObject enemyExplosion;


	public int scoreValue;
	public int hp;

	public GameController gameController;
	protected PlayerController playerController;
	protected DropManager dropManager;
	
	public virtual void Start()
	{
		GameObject gameControllerObject = GameObject.FindWithTag("GameController");
		GameObject playerControllerObject = GameObject.FindWithTag("Player");
		GameObject dropManagerObject = GameObject.FindWithTag("DropManager");
		
		if(gameControllerObject && playerControllerObject && dropManagerObject)
		{
			gameController = gameControllerObject.GetComponent<GameController>();
			playerController = playerControllerObject.GetComponent<PlayerController>();
			dropManager = dropManagerObject.GetComponent<DropManager>();
		}
		
		if(gameController == null || playerController == null)
		{
			Debug.Log ("Cannot find a required script");
		}
	}

	//Checks collision with other objects
	void OnTriggerEnter(Collider other) 
	{
		if(other.tag == "Shoot")
		{
			Destroy(other.gameObject);
			int damage = other.GetComponent<Shot>().damage;
			onDamage(damage);
		}

		if(other.tag == "Player")
		{
			onPlayerHit();
		}
	}

	//Abstract method. To be called when enemy takes damage
	protected abstract void onDamage(int damage);

	//Abstract method. To be called when hits player
	protected abstract void onPlayerHit();

	//Abstract method. To be called when enemy dies
	protected abstract void onDeath();
}
