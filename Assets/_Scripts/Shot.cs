﻿/**
 * Created by: Jorge Luiz Andrade
 * Last Update: March 17 2015
 * Purpose: Basic shot script. Just to store shot damage
 * */

using UnityEngine;
using System.Collections;

public class Shot : MonoBehaviour {
	public int damage;
}
