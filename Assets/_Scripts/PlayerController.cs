﻿/**
 * Created by: Jorge Luiz Andrade
 * Last Update: March 25 2015
 * Purpose: This is the script to control the player module. 
 * */

using UnityEngine;
using System.Collections;

[System.Serializable]
public class Boundary
{
	public float xMin, xMax, zMin, zMax;
}

public class PlayerController : MonoBehaviour 
{
	public float speed;
	public Boundary boundary;

	public GameObject playerExplosion;
	public GameObject shieldObject;

	private int shield;
	public int Shield { get { return this.shield; } }

	private Weapon weapon;
	private GameController gameController;

	private bool visibleShield;
	private float shieldTimer;

	void Start()
	{
		//Gets gameController script for future uses
		GameObject gameControllerObject = GameObject.FindWithTag("GameController");
		if(gameControllerObject)
		{
			gameController = gameControllerObject.GetComponent<GameController>();
		}
		
		if(gameController == null)
		{
			Debug.Log ("Cannot find GameController script");
		}

		//Initialize weapon with the basic weapon. Gets weapon script
		weapon = GetComponentInChildren<Weapon>();
		
		//Initilize shield value and change text on screen
		shield = 100;
		gameController.UpdateShieldText(shield);

		visibleShield = false;
	}

	void Update()
	{
		//Check fire input
		if((Input.GetButton("Fire1") || Input.GetKey(KeyCode.Space)) && weapon != null)
		{
			//Calls weapon base class shoot method
			weapon.Shoot();
		}

		if(visibleShield)
		{
			shieldTimer += Time.deltaTime;
			if(shieldTimer >= 0.3)
			{
				visibleShield = false;
				shieldObject.renderer.enabled = false;
				shieldTimer = 0;
			}
		}

	}

	void FixedUpdate()
	{
		float moveHorizontal = Input.GetAxis("Horizontal");
		float moveVertical = Input.GetAxis("Vertical");

		Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
		rigidbody.velocity = movement * speed;

		rigidbody.position = new Vector3
		(
			Mathf.Clamp(rigidbody.position.x, boundary.xMin, boundary.xMax),
			0.0f,
			Mathf.Clamp(rigidbody.position.z, boundary.zMin, boundary.zMax)
		);
	}

	//Checks player collisions
	void OnTriggerEnter(Collider other)
	{
		//Check if player got an weapon
		if(other.tag == "Weapon")
		{
			if(weapon != null)
				Destroy(weapon.gameObject);

			changeWeapon(other.gameObject);
		}else if(other.tag == "EnemyShot") //Check if player was hit by an enemy shot
		{
			int damage = other.GetComponent<Shot>().damage;
			OnDamage(damage);
			Destroy(other.gameObject);


		}else if(other.tag == "Shield")
		{
			shield += other.GetComponent<Shield>().shieldValue;
			if(shield > 100)
				shield = 100;
			gameController.UpdateShieldText(shield);
			Destroy(other.gameObject);
		}
	}

	//Fuction to be called when player takes damage. Reduces player hitpoints and checks death
	public void OnDamage(int damage)
	{
		shield -= damage;

		if(shield <= 0)
		{
			OnDeath();
			shield = 0;
		}else
		{
			shieldObject.renderer.enabled = true;
			shieldObject.audio.Play();
			visibleShield = true;
		}

		gameController.UpdateShieldText(shield);
	}

	//Function to be called when player dies. Shows player explosion, destroy object and sets game over
	private void OnDeath()
	{
		Instantiate (playerExplosion, transform.position, transform.rotation);
		Destroy(gameObject);
		gameController.GameOver();
	}

	public void changeWeapon(GameObject newWeapon)
	{
		weapon = newWeapon.GetComponent<Weapon>();
		weapon.shotPosition = this.transform.Find("ShotPosition").transform;
		gameController.UpdateWeapon(weapon.GetName());

		
		newWeapon.transform.SetParent(this.gameObject.transform);
		newWeapon.renderer.enabled = false;
		newWeapon.collider.enabled = false;
	}
	
}
